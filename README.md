<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Biserica Penticostală Română din Italia</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
<body style="background-color: aliceblue">
	 <header>
      <a href="index.html">
        <h1 align="center">Biserica Penticostală Română din Italia</h1>
        <h2 align="center">"Către aleşii care trăiesc ca străini"</h2>	
      </a>
 <style>
* {box-sizing: border-box;}

body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #2196F3;
  color: white;
}

.topnav input[type=text] {
  float: right;
  padding: 6px;
  margin-top: 8px;
  margin-right: 16px;
  border: none;
  font-size: 17px;
}

@media screen and (max-width: 600px) {
  .topnav a, .topnav input[type=text] {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
}
</style>
</head>
<body>

<div class="topnav" style="background-color: #aaa;">
       <a class="active" href="#acasă">Acasă</a>
       <a href="#scurtistoric">Scurt istoric</a>
       <a href="#conducere">Conducere</a>
       <a href="#departamente">Departamente</a>
       <a href="#misiune">Misiune</a>
       <a href="#adresabiserici">Adresa biserici</a>
       <a href="#slujitori">Slujitori</a>
       <a href="#evenimente/conferinte">Evenimente/Conferinţe</a>
       <a href="#intalniri">Întâlniri pe filială</a>
       <a href="#cauzespeciale">Cauze speciale</a>
       <a href="#contact">Contact</a>
      </div>
<input  type="text" placeholder="Search.." style="padding-top: 0px;width: 214px;height:31px;">
    </header>
	<p>Bine ați venit pe site-ul Bisericilor Penticostale Române din Italia!

       Dorim ca și prin acest mijloc de comunicare să vă informăm cu privire la evenimente din cadrul Comunității noastre,cu privire la date de contact ale bisericilor și ale conducerilor acestora, fotografii, imagini video, informații utile care să faciliteze comunicarea personală și directă a dvs. cu Bisericile Penticostale Române din Italia. </p>
       <img src="https://image.jimcdn.com/app/cms/image/transf/dimension=300x1024:format=jpg/path/s6ad55729bfc3308d/image/i64b44b11ee2d5d44/version/1291279090/image.jpg">
         <footer>
      <p>&copy; 2018 Cureluşă Marius.</p>
    </footer>  
</body>
